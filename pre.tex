% !TeX spellcheck = en_US
\setcounter{section}{-1}

\section{Introduction}

Derivatives, or more generally Taylor approximations, are a -- if not \emph{the} -- central concept in Analysis, differential geometry and actually most areas of Mathematics. At first it might seem that commutative algebra poses an exception, but a student that advances further into this area will soon find that, even though usually no limits of difference quotients are taken, the ideas of derivatives and first-order approximations appear in the form of \emph{Kähler differentials}. These are particularly important in algebraic geometry for characterizing smoothness, Serre duality or de Rham cohomology, for example. Derivations, their dual, provide the notion of first-order differential operators%
\footnote{A first order differential operator is actually the sum of a ``function'' and a derivation.}
. Higher order approximations and differential operators were introduced and studied by Grothendieck in \cite{ega4.4}.

This thesis is mainly a summary
of the corresponding chapter of \cite{ega4.4}. Compared to the original, we try to give more examples and interpretation, as well as more detailed proofs. 

We will first introduce (in \cref{sec:sheaves}) the \demph{sheaves of principal parts}
of oder $n$, which allow us to provide a $n$-th order infinitesimal approximation of sections of the structure sheaf and later of an arbitrary $\OO_X$-module.
Afterwards, we will be able to treat \demph{differential operators} (in \cref{sec:diffop}) on a (relative) Scheme and prove some of their fundamental properties.

We take this as an opportunity to introduce and examine the well-known \demph{sheaves of (Kähler) differentials}, since many of their important properties follow very quickly from corresponding properties of the (in a way more general) sheaves of principal parts.


Before investigating these sheaves, we will however have to treat their affine counterparts, the
\demph{algebra of principal parts} (in \cref{sec:Pn}) and the \demph{module of Kähler differentials} (in \cref{sec:kaehler}), in the setting of an algebra over a ring.

The examples provided are intended to illustrate the introduced constructions and provide the reader with interpretations of the somewhat abstract results. Most importantly we will show the analogies (but sometimes also the differences) to the correspondent concepts in the theory of smooth manifolds: (smooth) differential forms for Kähler differentials, jet bundles for the sheaves of principal parts and (smooth) differential operators for (algebraic) differential operators -- in fact, we will see at the end of \cref{sec:diffop} that these last two are actually two instances of a very general concept of differential operators on a ringed space.
Since the focus of this thesis lies on the algebraic side of things, we will be less detailed when giving analytic arguments.



\begin{notation}
	We consider $0 \in \NN$ and all rings to be commutative and unitary (unless stated otherwise).
\end{notation}


\begin{notation}[multiindices]
	A multiindex is a tuple $\alpha  = (\vars{\alpha}{n})\in \NN^n$. We define $|\alpha| = \alpha_1 + \dots + \alpha_n$ and $\alpha! = \alpha_1 ! \cdots \alpha_n !$. For two multiindices $\alpha$ and $\beta$, expressions like $\alpha \pm \beta$ or $\alpha \cdot \beta$ are to be understood component-wise. $a \geq \beta$ means $\forall i: \alpha_i \geq \beta_i$. We also set
	%
	\begin{equation}
		\binom{\alpha}{\beta} = \frac{\alpha!}{(\alpha - \beta)! \beta!} = \binom{\alpha_1}{\beta_1} \cdots \binom{\alpha_n}{\beta_n}
	\end{equation}
	%
	Furthermore, if elements $\vars{x}{n}$ of some ring are defined, we write
	%
	\begin{equation}
		\underline{x}^\alpha = {x_1}^{\alpha_1} \cdots {x_n}^{\alpha_n} \p
	\end{equation}
	%
\end{notation}

\begin{notation}[partial derivatives]
	We use the shorthand 
	%
	\begin{equation}
		\partial_i = \frac{\partial}{\partial x_i}
	\end{equation}
	%
	when a variable (in some polynomial ring) or coordinate $x_i$ is defined. For a multiindex $\alpha \in \NN^n$ we also write
	%
	\begin{equation}
		\partial^\alpha = {\partial_1}^{\alpha_1} \cdots {\partial_n}^{\alpha_n}
		= \frac{\partial^{|\alpha|}}{\partial {x_1}^{\alpha_1} \cdots \partial {x_n}^{\alpha_n}} \p
	\end{equation}
	%
\end{notation}

\begin{notation}[commutators]
	If $N$ is a noncommutative ring, we write $[a, b] = a b - b a$ for $a, b \in N$.
\end{notation}
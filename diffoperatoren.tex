% !TeX spellcheck = en_US
\section{Differential Operators} \label{sec:diffop}
Again, let $X$ be an $S$-Scheme via the structure morphism $f: X \to S$ for the rest of this section. 

We now have the tools necessary give the definition of differential operators on a Scheme and prove some of their properties. We only want to consider what is usually called a \emph{linear} differential operator, so something like $t \mapsto (t')^2$ for ``functions'' $t$ lies outside of our scope. We will give a definition of differential operators in terms of the sheaves of principal parts $\PP^n_{X/S}$ but in \cref{thm:diffop_alt} we will see that there is another more general, simple and elegant definition. However, unlike the definition we will use, that definition has the drawback that it is not very useful for proving statements about the set of all differential operators on a scheme.


\begin{defn} \label{def:diffop} \def\currentprefix{def:diffop}
	\begin{enumerate}[(i)]
	\itemsep 0pt
		\item Let $\FF$ and $\GG$ be two $\OO_X$-modules and $n \in \NN$. A homomorphism of sheaves of abelian groups $D: \FF \to \GG$ is called a \demph{differential operator of order} $\leq n$ if there is a $\OO_X$-linear map $u: \PP^n_{X/S}(\FF) \to \GG$ such that $D = u \circ T^n_{\FF}$.
		The abelian group of differential operators of order $\leq n$ from $\FF$ to $\GG$ is denoted by $\Diff_{X/S}^n(\FF, \GG)$. \llabel{loc}
		%
		\item A morphism of sheaves of abelian groups $D: \FF \to \GG$ is called a \demph{differential operator} (of unspecified order) if there is an open cover $X = \bigcup_i U_i$ and $n_i \in \NN$ such that $D|_{U_i} \in \Diff_{X/S}^{n_i}(\FF, \GG)$ for every $i$. The abelian group of differential operators from $\FF$ to $\GG$ is denoted by $\Diff_{X/S}(\FF, \GG)$ or $\Diff^\infty_{X/S}(\FF, \GG)$.
		%
		\item The \demph{order} of a differential operator $D$, denoted by $\ord D$, is the smallest $n \in \NN \cup \{\infty\}$ for which there is such a cover with $n_i \leq n$.%
		\footnote{At this moment it is not clear that this coincides with the definition of order implicit in \lref{loc}. We need to check that if there is a cover giving $D$ order $n$, there will be a \emph{global} $u: \PP^n_{X/S}(\FF) \to \GG$ such that $D$ factors as $D = u \circ \T^n_\FF$. See \cref{rem:diffop_deg_consistent}.}
		\llabel{glob}
	\end{enumerate}
	\noindent
	We just write $\Diff^n_{X/S}$ for $\Diff^n_{X/S}(\OO_X, \OO_X)$. We consider the zero morphism $0: \FF \to GG$ a differential operator of order $-\infty$.
\end{defn}

\begin{rem}[trivialities]
	For $n, m \in \NN \cup \{\infty\}$ with $m \geq n$, we have canonical surjective homomorphisms $\PP^m_{X/S}(\FF) \to \PP^n_{X/S}(\FF)$ compatible with $\T^m_\FF$ and $\T^n_\FF$. Hence, $\Diff_{X/S}^n(\FF, \GG) \subset \Diff_{X/S}^m(\FF, \GG)$.
	Differential operators of order $0$ are simply $\OO_X$-homomorphisms (because $\T^0_\FF = \id_\FF$). In particular $\Diff^0_{X/S} \cong \OO_X(X)$.
	
	Since $\T^n_{X/S}$ is $f^{-1} \OO_S$-linear, every differential operator on $X$ is $f^{-1} \OO_S$-linear.
\end{rem}

\begin{rem}[local description] \label{rem:diffop_local} 
	Let $\FF$, $\GG$ be two $\OO_X$-modules and $D: \FF \to \GG$ a $f^{-1} \OO_S$-linear morphism.
	%
	\begin{equation}
		\mathcal{B} = \{ U \subset X \mid U \text{~affine open}, \exists V \subset S \text{~affine open}: f(U) \subset V\}
	\end{equation}
	%
	is a basis of  the topology of $X$. Hence $D$ is uniquely determined by $D(U) : \FF(U) \to \GG(U)$ for $U \in \mathcal{B}$. Set $A = \OO_X(U)$, $R = \OO_S(V)$, $M = \FF(U)$ and $N = \GG(U)$ and denote the presheaf which we obtain in \cref{eq:def_PnF} if we don't sheafify for the tensor product by $\PP^n_{X/S}[\FF]$. Then
	%
	\begin{align}
		\Gamma\left( \PP^n_{X/S}[\FF], U \right) &= P^n_{A/R} \otimes_A M \\
		&= \left( (A \otimes_R A) / I^{n+1}_{A/R} \right) \otimes_A M \p
	\end{align}
	%	
	where the $A$-tensor product in the second line is formed w.\,r.\,t.\ $T^n_{A/R}$.
	We perceive this
	as an $A$-module via the first factor in the first tensor product.
	This can be simplified further using that there is a natural isomorphism of $A$-modules
	%
	\begin{align}
		\left( (A \otimes_R A) / I^{n+1}_{A/R} \right) \otimes_A M &\longiso A \otimes_R M / I^{n+1}_{A/R} (A \otimes_R M) \\
		(\ovl{a \otimes b}) \otimes m  &\longmapsto  \ovl{a \otimes b m}
	\end{align}
	%
	(both domain and codomain being an $A$-module via the first factor of the tensor products).
	$D$ is a differential operator of order $\leq n$ iff
	there is a morphism of presheaves $u : \PP^n_{X/S}[\FF] \to \GG$ such that
	$D(U): M \to N$ factors as $u(U) \circ T^n_{A/R}$ for every $U$. This is equivalent to saying that $D(U)$ factors as
	%
	\begin{align}
		M & \longto A \otimes_R M / I^{n+1}_{A/R} (A \otimes_R M) \xrightarrow{~K_{D(U)}~} N\\
		m &\longmapsto \ovl{1 \otimes m} \p
	\end{align}
	%
	for some morphism of presheaves $K_D$.
	%Since for every $D$ we have a canonical $A$-linear map $L_{D(U)}: A \otimes_R M \to N, a \otimes m \mapsto a D(U)(m)$, the above is equivalent to saying that $L_{D(U)}$ is zero on $I^{n+1}_{A/R} (A \otimes_R M)$.
	The $A$-linearity of $K_D(U)$ and the factorization condition imply that $K_D(U)(\ovl{a \otimes m}) = a D(U)(m)$. Hence, $K_D(U)$ is necessarily the map induced on $U$ by by the $\OO_X$-linear morphism of presheaves $L_D$ given by
	$L_D(U): A \otimes_R M  \to N, a \otimes m \mapsto a D(U)(m)$. Since $D(U)$ is $R$-linear, $L_D$ is well-defined (regardless of whether $D$ is a differential operator).
	Therefore, the condition that $D$ is a differential operator is equivalent to $L_D(U)$ being zero on $ I^{n+1}_{A/R} (A \otimes_R M)$ for every $U$.
\end{rem}

% ----------------------------------------------------------------------------------------

Let $\FF$, $\GG$ be as above and $n \in \NN \cup \{\infty\}$. For $U \subset X$ open we have homomorphisms of abelian groups $\Diff^n_{X/S}(\FF, \GG) \to \Diff^n_{U/S}(\FF|_U, \GG|_U)$, showing that $U \mapsto \Diff^n_{X/S}(\FF, \GG)$ is a presheaf, which we will denote by $\sDiff^n_{X/S}(\FF, \GG)$. In fact, this is a sheaf, as the following proposition shows:

\begin{prop} \label{thm:Diffop_Hom}
	Let $\FF$, $\GG$ be $\OO_X$-modules and $n \in \NN \cup \{\infty\}$. Then precomposition with $\T^n_\FF$ yields a morphism of presheaves of abelian groups
	%
	\begin{equation}
		\sHom_{\OO_X} \left( \PP^n_{X/S}(\FF), \GG \right) \longiso \sDiff^n_{X/S} (\FF, \GG ) \label{eq:Diff_Hom}
	\end{equation}
	%
	which is an isomorphism. In particular, $\sDiff^n_{X/S}(\FF, \GG)$ is a sheaf.
\end{prop}
\begin{proof}
	From the definition of $\sDiff^n_{X/S}(\FF, \GG)$ it is clear that $- \circ T^n_\FF$ is a morphism of presheaves that is surjective on every open set. Now let $U \subset X$ be open and $u, v \in \Hom_U(\PP^n_{U/S}(\FF|_U), \GG|_U)$ such that $u \circ T^n_{U/S} = v \circ T^n_{U/S}$. This implies that $u$ and $v$ agree on $\im T^n_{U/S}$, which generates $\PP^n_{U/S}$ by \cref{thm:Pn_sheaf_gen} and hence $u = v$ by \cref{thm:sheaf_gen}(\ref{thm:sheaf_gen:det}). This shows that $- \circ T^n_{\FF}$ is injective on every open subset of $X$, so the claim follows.
\end{proof}


\begin{rem} \label{rem:diffop_deg_consistent}
	The above Proposition shows that the two notions of the order of a differential operator given in \cref{def:diffop} coincide. If $D: \FF \to \GG$ is locally given by sections of $\sDiff^n_{X/S}(\FF, \GG)$, they glue to a section in $\Diff^n_{X/S}(\FF, \GG)$.
\end{rem}


\begin{defn} \label{def:op_mult}
	For $\OO_X$-modules $\FF$ and $\GG$, the sheaves of abelian groups $\sHom_{\ZZ} \left( \FF, \GG \right)$ (morphisms of sheaves of abelian groups) and $\sHom_{f^{-1} \OO_S}\left( \FF, \GG \right)$ carry \emph{two} canonical structures of an $\OO_X$-module, which we will denote by left- and right-multiplication. On some open $U \subset X$ and for $D: \FF|_U \to \GG|_U$, $a \in \OO_X(U)$, they are given by
	%
	\begin{align}
		(a D) (t) &= a (D(t)) \\
		(D a) (t) &= D (a t) 
	\end{align}
	%
	where $t \in \FF(V)$ for some open subset $V \subset U$, using the $\OO_X$-module structures of $\GG$ and $\FF$ respectively.
\end{defn}

\begin{rem} \label{rem:diffop_bimod}
	The sheaves of differential operators $\sDiff^n_{X/S}(\FF, \GG)$, $n \in \NN \cup \{\infty\}$ are $\OO_X$-submodules with respect to both $\OO_X$-module structures on $\sHom_{f^{-1} \OO_S}\left( \FF, \GG \right)$: If $D = u \circ T^n_\FF$ as in \cref{def:diffop}, then $a D = (a u) \circ T^n_\FF$ is a differential operator and so is
	%
	\begin{equation}
		D a = u \circ T^n_\FF (a \cdot -) = u \circ \left( T^n_{X/S}(a) T^n_\FF \right) = u\left( T^n_{X/S}(a) \cdot - \right) \circ T^n_\FF \c
	\end{equation}
	%
	because $u( T^n_{X/S}(a) \cdot - )$ is obviously $\OO_X$-linear.
	We see that the left-module structure comes from the canonical $\OO_X$-module structure of $\PP^n_{X/S}(\FF)$ in \cref{eq:Diff_Hom}, while the right-module structure comes from the $\OO_X$-module structure of $\PP^n_{X/S}(\FF)$ via $T^n_{X/S}$.%
\end{rem}

% ----------------------------------------------------------------------------------------

Again, we ask the question whether the $\OO_X$-modules we have defined are quasi-coherent:

\begin{prop}[{\cite[16.8.6]{ega4.4}}] \label{thm:diffop_qcoh}
	Suppose $f$ is locally of finite presentation (e.\,g.\ $X/S$ is smooth), $\FF$ and $\GG$ are quasi-coherent $\OO_X$-modules and $\FF$ is locally of finite presentation. Then $\sDiff^n_{X/S}(\FF, \GG)$ is a quasi-coherent $\OO_X$-module with respect to 
	its left-module structure.
	%both its left- and right-module structures.
\end{prop}

\begin{proof}
	%For the left-module structure 
	The claim follows from \cref{thm:Diffop_Hom,thm:PnF_presentations}, using the fact that the $\sHom$ of a locally finitely presented and a quasi-coherent module is quasi-coherent.
	%
	%For the case of the right-module structure we encounter the difficulty that, while $\sHom_{\OO_X} \left( \PP^n_{X/S}(\FF), \GG \right)$ consists of module homomorphisms w.\,r.\,t.\  the conventional module structure on $\PP^n_{X/S}(\FF)$, we want to treat it as a $\OO_X$-module w.\,r.\,t.\ the module structure on $\PP^n_{X/S}(\FF)$ induced by $T^n_{\FF}$. To circumvent this, consider the $\OO_X$-module $\mathcal{S} \coloneqq \PP^n_{X/S} \otimes_{\OO_X} \FF$, where the tensor product is formed w.\,r.\,t.\ the \emph{conventional} $\OO_X$-module structure on $\PP^n_{X/S}$, but we view it as $\OO_X$-module via the structure on $\PP^n_{X/S}$ by $T^n_{X/S}$. This is isomorphic as $\OO_X$-module to $\PP^n_{X/S}(\FF)$ via $\sigma \otimes \id_{\FF}$ (see \cref{rem:symmetry}).
\end{proof}

% ----------------------------------------------------------------------------------------

\begin{ex}[differential operators on $\AA^n_R$] \label{ex:diffop_affinespace}
	Let $R$ be a ring and set $A = R[\vars{x}{n}]$, $S = \Spec R$ and $X= \AA^n_R = \Spec A$. By \cref{rem:interp_Pn} and \cref{thm:Pn_qcoh}, $\PP^k_{X/S}$ is the quasi-coherent module on $X$ associated to $P^k_{A/R} = A[\vars{\epsilon}{n}] / (\vars{\epsilon}{n})^n$. By \cref{thm:diffop_qcoh}, $\sHom_{\OO_X}(\PP^k_{X/S}, \OO_X)$ is quasi-coherent and corresponds to the $A$-module
	%
	\begin{equation}
		\Hom_A \left( A[\vars{\epsilon}{n}]/(\vars{\epsilon}{n})^k, A \right) \cong
		\bigoplus_{\substack{\alpha \in \NN^n \\ |\alpha| \leq k}} A \p
	\end{equation}
	%
	Since $T^k_{X/S}$ maps rational functions to their Taylor polynomials of order $k$ evaluated at $\vars{\epsilon}{n}$, we conclude that
	%
	\begin{equation}
		\sDiff^k_{X/S} = \bigoplus_{\substack{\alpha \in \NN^n \\ |\alpha| \leq k}} \OO_X \dred{\alpha} \label{eq:diffop_affine_expl}\c
	\end{equation}
	%
	using the convention from \cref{eq:derivative_charp}. Since $\AA^n_R$ is quasi-compact every differential operator on it has finite order. Therefore we obtain
	%
	\begin{equation}
		\sDiff_{X/S} = \bigcup_{k \in \NN} \sDiff^k_{X/S}
					 = \bigoplus_{\alpha \in \NN^n} \OO_X \dred{\alpha}
	 	\label{eq:diffop_affine_expl2}
		\p
	\end{equation}
	%
	We would expect that the order of a differential operator on $\AA^n_R$ is simply the order of the highest derivative(s) appearing in the representation given by \cref{eq:diffop_affine_expl2}. This boils down to checking that for multiindices $\alpha^{(1)}, \dots, \alpha^{(m)}$ with $|\alpha^{(i)}| \leq k$, $|\alpha^{(1)}| = k$ and $g_1, \dots, g_m \in A \setminus \{0\}$,
	%
	\begin{equation}
		\ord D = k \,, \text{~where~} D = g_1 \dred{\alpha^{(1)}} + \dots + g_m \dred{\alpha^{(m)}} \p
	\end{equation}
	%
	We already know that $\ord D \leq k$. $\ord D < k$ would mean that 
	%there is a $A$-linear $u : P^{k-1}_{A/R} \to A$ such that $u \circ \T^{k-1}_{A/R} = D$. By precomposing with the canonical map $P^k_{A/R} \to P^{k-1}_{A/R}$
	$D$ corresponds (via \cref{thm:Diffop_Hom}) to a $v: P^{k}_{A/R} \to A$ vanishing on $(\underline{\epsilon}^\alpha \mid \alpha \in \NN^n, |\alpha| = k)_A$. But $D$ already corresponds to a $u: P^{k}_{A/R} \to A$ mapping $\underline{\epsilon}^{\alpha^{(1)}}$ to $g_1 \neq 0$, hence such a $v$ cannot exist by the injectivity of the morphism in \cref{eq:Diff_Hom}.
\end{ex}

% ----------------------------------------------------------------------------------------

\begin{ex}
	Not every $f^{-1} \OO_S$-linear morphism of $\OO_X$-modules is a differential operator:
	
	Consider $S = \Spec k$ for some field $k$ and $X = \AA^1_k$. Since $X$ is integral, a ($k$-linear) morphism of sheaves $\Phi: \OO_X \to \OO_X$ corresponds to a ($k$-linear) endomorphism $\phi : k(x) \to k(x)$ such that for every open $U \subset X$ we have $\phi(\OO_X(U)) \subset \OO_X(U)$. Algebraically this means that for every prime $p \in k[x]$ and every $t \in k(x)$, its image $\phi(t)$ may only have a pole%
	\footnote{We say that $s \in k(x)$ ha a pole at $p$ if $v_p(s) < 0$, where $v_p$ is the valuation of $k(x)$ w.\,r.\,t.\ $p$.}
	at a prime $p \in k[x]$ if $t$ also has a pole at $p$.
	
	Using polynomial division, any $t \in k(x)$ has a unique representation $t = q + \rho$ with $q \in k[x]$ and $\deg \rho < 0$. The mapping $\fl$ sending $t \mapsto q$ satisfies the above conditions to induce a morphism of sheaves (which we will also denote by $\fl$). Since $\deg(\rho + \sigma) \leq \max\{\deg \rho, \deg \sigma\}$ for all $\rho, \sigma \in k(x)$, $\fl$ is additive and even $k$-linear. 
	
	Suppose $\fl$ is a differential operator. Then by \cref{ex:diffop_affinespace}, $\fl$ is of the form
	%
	\begin{equation}
		\fl = f_0 + f_1 \partial + f_2 \partial^2 + \cdots + f_n \partial^n, \quad f_i \in k[x], f_n \neq 0 \p
	\end{equation}
	%
	So $\floor{1} = 1$ implies that $f_0 = 1$. But now $\floor{x} = x$ implies $f_1 = 0$ and using $\floor{x^m} = x^k$ for $m \geq 0$ we inductively obtain that $f_m = 0$ for $m \geq 0$. Hence $\fl = \id_{k(x)}$, contradicting $\floor{x^{-1}} = 0$.
	
	One can actually observe that for a system of of primes $\mathbb{P}$ for $k(x)$,
	%
	\begin{equation}
		\{1, x, x^2, \dots\} \cup \left\{\frac{x^k}{p^n} ~\middle|~ p \in \mathbb{P}, n \in \NN^+, k < \deg p \right\}
	\end{equation}
	%
	is a $k$-vector space basis of $k(x)$  via partial fraction decomposition. The $k$-endomorphisms of $k(x)$ that induce a map of sheaves are those in which basis elements of the form $x^k/p^n$ are mapped to linear combinations of $\{1, x, x^2, \dots\} \cup \{x^\l / p^m \mid m \in \NN^+, \l < \deg p\}$ and basis elements of the form $x^k$ are mapped to polynomials. Evidently, not all of these endomorphisms correspond to differential operators.
\end{ex}

This is a significant difference to the setting of differentiable manifolds: There we have Peetre's Theorem

\begin{thm}[\cite{peetre}] \label{thm:peetre}
	Let $M$ be a $n$-dimensional smooth manifold. Then any $\RR$-linear morphism of sheaves $\Phi: C^\infty_M \to C^\infty_M$ is a differential operator i.\,e.\ there exists an atlas for $M$ such that in every of its charts $(U, (x_i)_i)$
	%
	\begin{equation}
		P|_U = \sum_{\alpha \in \NN^n} g^\alpha \partial^\alpha \label{eq:diffop_explicit}
	\end{equation}
	%
	for some family of smooth functions $g^\alpha \in C^\infty_M(U)$ of which only finitely many are nonzero.
\end{thm}

\begin{rem}
	Since vector bundles on $M$ are locally isomorphism to $M \times \RR^k$, the theorem immediately generalizes to morphisms between the sheaves of sections of vector bundles.
\end{rem}
\begin{rem}
	Instead of formulating the theorem for morphisms of sheaves $\Phi: C^\infty_M \to C^\infty_M$, it is often stated for $\RR$-linear operators $\phi: C^\infty_M(M) \to C^\infty_M(M)$ with the property that $\forall u \in C^\infty_M(M): \supp \phi(u) \subset \supp u$. This condition is obviously satisfied by the map on global sections $\Phi(M)$ of a morphism of sheaves $\Phi$.
	On the other hand, by the existence of bump functions on a smooth manifold, such an operator $\phi$ actually extends to a morphism of sheaves $\hat{\phi}: C^\infty_M \to C^\infty_M$ such that $\hat{\phi}(M) = \phi$, so the two formulations are equivalent.
\end{rem}

% ----------------------------------------------------------------------------------------

The following proposition gives us two more elegant characterizations of differential operators of a given order: The first one is a recursive characterization that generalizes derivations (which are just special differential operators of order 1).
The second one is just a restatement of the first (their equivalence follows from manipulation of symbols) in which we unfold the recursion.


\begin{prop}[{\cite[16.8.8]{ega4.4}}] \label{thm:diffop_alt} \def\currentprefix{thm:diffop_alt}
	Let $\FF$ and $\GG$ be $\OO_X$-modules, $D: \FF \to \GG$ a $f^{-1} \OO_S$-linear morphism and $n \in \NN$. Then the following are equivalent
	%
	\begin{enumerate}[(a)]
		\item $D$ is a differential operator of order $\leq n$. \llabel{normal}
		\item For every open $U \subset X$ and $a \in \OO_X(U)$, the morphism $[D, a] = D a - a D$ is a differential operator of order $\leq n-1$. \llabel{comm}
		\item For every open $U \subset X$, $a_0, \dots, a_n \in \OO_X(U)$ and $t \in \FF(U)$, we have that
		%
		\begin{equation}
			\smashoperator{\sum_{H \subset \{0, \dots, n\}}}
					(-1)^{|H|} \left( \prod_{i \in H} a_i \right) D\left( t \prod_{i \not\in H} a_i\right) = 0 \p \label{eq:diffop_multicomm}
		\end{equation}
		%
		\llabel{multi}
	\end{enumerate}
	%
\end{prop}
\begin{proof}
	``\lref{normal} $\Leftrightarrow$ \lref{multi}'' Since they form a basis of the topology and $\GG$ is a sheaf, it suffices to check \cref{eq:diffop_multicomm} on all affine opens lying in the preimage of an affine open of $S$.
	
	Let $V = \Spec R \subset S$, $U = \Spec A \subset f^{-1}(V)$ and set $M = \FF(X)$, $N = \GG(X)$. From \cref{rem:diffop_local} (using the notation from there), we know that \lref{normal} is equivalent to $L_D(U): A \otimes_R M \to N$ being zero on $I^{n+1}_{A/R} (A \otimes_R M)$ for every such $U$. Using \cref{thm:gen} we can restate this as $L_D(U)$ being zero on elements of the form
	%
	\begin{equation}
		\left( \prod_{i = 0}^n a_i \otimes 1 - 1 \otimes a_i \right) (1 \otimes m)  = 
		\;\smashoperator{\sum_{H \subset \{0, \dots, n\}}}
							(-1)^{|H|} \left( \prod_{i \in H} a_i \right) \otimes \left( t \prod_{i \not\in H} a_i\right)
	\end{equation}
	%
	for every such $U$, where $a_i \in A$ and $m \in M$. By the definition of $L_D$, this is equivalent to $(c)$.
	
	``\lref{comm} $\Leftrightarrow$ \lref{multi}''. We proceed by inducting over $n$. For $n = 0$, both \lref{comm} and \lref{multi} are equivalent to $a D - D a = 0$ for every $U \subseteq X$ open and $a \in \OO_X(U)$. Now let $n \geq 1$. Using the induction hypothesis that \lref{comm} and \lref{multi} are equivalent for $n-1$, we obtain that, for $n$, \lref{comm} is equivalent to the condition
	%
	\begin{gather}
		\forall U \subset X \text{~open}, a \in \OO_X(U),\;\, U' \subset U \text{~open}, a_0, \dots, a_{n-1} \in \OO_X(U'), t \in \FF(U'): \\
		\smashoperator{\sum_{H \subset \{0, \dots, n-1\}}} (-1)^{|H|}  \left( \prod_{i \in H} a_i \right) (D a - a D) \left( t \prod_{i \not\in H} a_i\right) = 0 \c \label{eq:multi_of_comm}
	\end{gather}
	%
	which we shall denote by (b').
	Observe that the above doesn't loose any generality if one always sets $U' = U$. Distributing over the difference shows that the left-hand side of \cref{eq:multi_of_comm} is equal to
	\begin{equation}
	\smashoperator{\sum_{H \subset \{0, \dots, n-1\}}} (-1)^{|H|}  \left( \prod_{i \in H} a_i \right) D \left( t a \prod_{i \not\in H} a_i\right) 
	- \;\smashoperator{\sum_{H \subset \{0, \dots, n-1\}}} (-1)^{|H|}  \left( a \prod_{i \in H} a_i \right) D \left( t \prod_{i \not\in H} a_i\right) \c
	\end{equation}
	which, if we rename $a$ to $a_n$ is the left-hand side of \cref{eq:diffop_multicomm}. Hence we see that (b') is equivalent to \lref{multi}.
\end{proof}

\begin{rem}
	The last part of our proof actually shows that 
	\begin{equation}
		\smashoperator{\sum_{H \subset \{0, \dots, n - 1\}}}
				(-1)^{|H|} \left( \prod_{i \in H} a_i \right) [D, a_n] \left( t \prod_{i \not\in H} a_i\right) = 
		\smashoperator{\sum_{H \subset \{0, \dots, n\}}}
				(-1)^{|H|} \left( \prod_{i \in H} a_i \right) D\left( t \prod_{i \not\in H} a_i\right) = 0 \p
	\end{equation}
	From this we get inductively that
	%
	\begin{equation}
		[ \, [ \cdots [ \, [D, a_n], a_{n-1}] \cdots, a_1], a_0] 
		= \smashoperator{\sum_{H \subset \{0, \dots, n\}}}
				(-1)^{|H|} \left( \prod_{i \in H} a_i \right) D\left( t \prod_{i \not\in H} a_i\right) \c
	\end{equation}
	%
	making the equivalence \cref{thm:diffop_alt:comm} $\Leftrightarrow$ \cref{thm:diffop_alt:multi} obvious.
\end{rem}

\begin{rem}
	Together with $\ord 0 = -\infty$, \cref{thm:diffop_alt:comm} actually gives us a far more elegant definition of differential operators on any ringed space $X$. In particular, if $X$ is a smooth manifold and we consider $\RR$-linear mappings between the sheafs of sections of vector bundles, by \hyperref[thm:peetre]{Peetre's Theorem} we obtain the usual (smooth) differential operators.
	
	The following three Corollaries only rely on \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm} to characterize differential operators and are hence valid on any ringed space.
\end{rem}


%\begin{ex}[continues=ex:diffop_affinespace]
%	We would expect that the order of a differential operator on $\AA^n_R$ is simply the order of the highest derivative(s) appearing in the representation given by \cref{eq:diffop_affine_expl}. This boils down to checking that $\frac1{\alpha!} \partial^\alpha$ is a differential operator of order $k = |\alpha|$ for any $\alpha \in \NN^n$. We already know that $\ord \frac1{\alpha!} \partial^\alpha \leq |\alpha|$. On the other hand, observe that $\partial^\alpha \underline{x}^\alpha = 1$ while $\partial^\alpha \underline{x}^{\alpha - \beta} = 0$ for any $\beta \in \NN^n \setminus \{\underline{0}\}$. Therefore choosing $a_0, \dots a_{k-1}$ as the $x_i$ appearing in $\underline{x}^\alpha = {x_1}^{\alpha_1} \cdots {x_n}^{\alpha_n}$ (with multiplicity) and $t = 1$ results in
%	%
%	\begin{equation}
%		\smashoperator{\sum_{H \subset \{0, \dots, k-1\}}}
%				(-1)^{|H|} \left( \prod_{i \in H} a_i \right) \partial^\alpha \left( t \prod_{i \not\in H} a_i\right) = \partial^\alpha \underline{x}^\alpha = 1 \p
%	\end{equation}
%	%
%	Hence, by \cref{thm:diffop_alt}, we see that $\ord \partial^\alpha \geq k$, confirming our expectations.
%\end{ex}
% ----------------------------------------------------------------------------------------

Anyone who has studied analysis knows that the algebraic structure of the set of differential operators on some space (say $\RR^n$, or a manifold) is richer than what we have see until now for $\sDiff_{X/S}$. Most importantly, they form a noncommutative ring with composition as multiplication. The algebraic case does not lack this structure, as the following corollaries show.

\begin{cor}[{\cite[16.8.9]{ega4.4}}] \label{thm:diffop_prod}
	Let $\FF$, $\GG$, $\HH$ be $\OO_X$-modules, $D \in \Diff^n_{X/S}(\FF, \GG)$ and $E \in \Diff^n_{X/S}(\GG, \HH)$ for some $n, m \in \NN \cup \{\infty\}$. Then $E \circ D$ is a differential operator of order $\leq n + m$, which we shall simply write as $E D$.
\end{cor}
The central element of the proof given in \cite{ega4.4} is \cref{thm:higher_der}. We give a more general proof based on \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm}.
\begin{proof}
	For the cases $m, n \in \NN$ we induct on both $n$ and $m$. Our base cases are those with $m = 0$ or $n = 0$, for which the claim was already verified in \cref{rem:diffop_bimod}.
	As induction step we show that if the statement holds for $(n-1, m)$ and $(n, m-1)$, it also holds for $(n, m)$. For an arbitrary section $a$ on some open of $X$, we calculate
	%
	\begin{align}
		[E D, a](t) &= E (D (a t)) - a E D(t) \\
					&= E \big( [D, a](t) + a D(t) \big) - a E D(t) \\
					&= (E [D, a])(t) + (E a) D(t) - (a E) D (t) \\
					&= (E [D, a])(t) + ([E, a] D) (t) 
					\p
	\end{align}
	%
	Since $[D, a]$ and $[E, a]$ have order $n-1$ and $m-1$ respectively by \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm}, $[E D, a]$ is a differential operator of order $n+m-1$ by the induction hypothesis. Since $E D$ is $f^{-1} \OO_S$-linear, by \cref{thm:diffop_alt} this shows that it is a differential operator.
	
	The cases in which $m = \infty$ or $n = \infty$ follow from those where $m, n \in \NN$ by looking at a cover $X = \bigcup_i U_i$ such that $D|_{U_i}$ and $E|_{U_i}$ have finite order for every $i$.
\end{proof}

\begin{cor}
	For any $\OO_X$-module $\FF$, $\sDiff^n_{X/S}(\FF, \FF)$ is a sheaf of noncommutative rings.
\end{cor}

% ----------------------------------------------------------------------------------------
In this setting, we obtain a generalization of \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm}
\begin{cor}
	For two differential operators $D, E \in \Diff_{X/S}(\FF, \FF)$, where $\FF$ is some $\OO_X$-module, we have that $\ord [E, D] < \ord E + \ord D$.
\end{cor}
\begin{proof}
	Using \cref{thm:diffop_prod}, we immediately see that $\ord [E, D] \leq \ord E + \ord D$.
	
	We again induct on $\ord E$ and $\ord D$ simultaneously. For $\ord E = 0$ or $\ord D = 0$ the claim follows from \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm}. Now let $\ord E, \ord D \geq 1$.
	In noncommutative rings the commutator satisfies the Jacobi identity, so for any $a \in \OO_X(X)$ we obtain
	%
	\begin{equation}
		[[E, D], a] = [[a, D], E] + [[E, a], D] \p
	\end{equation}
	%
	By the induction hypothesis and \cref{thm:diffop_alt}\cref{thm:diffop_alt:comm}, both summands in the right-hand side are differential operators of order $< E + D - 1$. We then obtain the claim then by applying \cref{thm:diffop_alt} again.
\end{proof}


% ----------------------------------------------------------------------------------------

\begin{ex}
	For $p \in \NN$, the \demph{sheaf of $p$-differentials} of $X$ relative to $S$ is defined as
	%
	\begin{equation}
		\Omega^p_{X/S} = \bigwedge^p \Omega^1_{X/S} \p
	\end{equation}
	%
	These form the homogeneous parts of the exterior algebra $\Omega^\bullet_{X/S} = \bigwedge\Omega^1_{X/S} = \bigoplus_{p \in \NN} \Omega^p_{X/S}$,
	see \cite[16.6]{ega4.4} for details. The differential $\d^1: \OO_X \to \Omega^1_{X/S}$ then generalizes to an additive morphism $d: \Omega^\bullet_{X/S} \to \Omega^\bullet_{X/S}$ given on open $U \subset X$ by\footnote{When we write $\d a$ this is always to be understood as $\d(a)$ and not as right multiplication to an operator in the sense of \cref{def:op_mult}}
	%
	\begin{equation}
		\d \left( h \d^1 g_1 \wedge \dots \wedge \d^1 g_p \right) = \d^1 h \wedge \d^1 g_1 \wedge \dots \wedge \d^1 g_p~,
		\qquad h, \vars{g}{p} \in \OO_X(U)
		\p
		\label{eq:deRham_d}
	\end{equation}
	%
	Since $\d^1$ is $f^{-1} \OO_S$-linear, $\d$ is so, too. Naturally the question arises whether $\d$ is a differential operator, and the answer is positive. On any open $U \subset X$ and for every $a \in \OO_X(U)$ and $\omega \in \Omega^\bullet_{X/S}(U)$, the product rule for $\d^1$ implies $\d (a \omega) = a \d \omega + (\d^1 a) \omega$. Hence we obtain
	%
	\begin{equation}
		[d, a] (\omega) = \d(a \omega) - a \d \omega = (\d^1 a) \omega \p
	\end{equation}
	%
	Since $\d^1\mkern -1mu (a) \in \OO_X(U)$ is a differential operator of oder zero, we deduce from \cref{thm:diffop_alt} that $\d$ is a differential operator of order 1.
\end{ex}

% ----------------------------------------------------------------------------------------

\begin{rem}
	Let us revisit the differential operators on $\AA^n_R$ from \cref{ex:diffop_affinespace}. We observe the well-known commutation relations
	%
	\begin{equation}
		\left[ \partial_i, \partial_j \right] = 0\,, \quad [x_i, x_j] = 0\,, \quad \left[ \partial_i, x_j \right] = \delta_{ij} \qquad \text{for $1 \leq i, j \leq n$.} \label{eq:canonical_comm}
	\end{equation}
	%
	If $R$ is a $\QQ$-algebra (so in particular if $R$ is a field of characteristic zero) dividing by $\alpha!$ is possible for any multiindex $\alpha$, so we see that $\sDiff_{X/S}$ is generated as a noncommutative $\OO_X$-algebra%
	\footnote{This is actually not a noncommutative algebra as defined by many authors, since they require a noncommutative (left) $A$-algebra $B$ to satisfy $(a x) y = x (a y)$ for $a \in A$, $x, y \in B$. Here we use the definition that a noncommutative $A$-algebra $B$ is a noncommutative ring equipped with a homomorphism of noncommutative rings $A \to B$. This is only equivalent to the usual definition if the image of the structure homomorphism lies in the center of $B$.}
	%(from the left) 
	by $\vars{\partial}{n}$. In fact, $\Diff_{X/S}$ is the $n$-th \demph{Weyl-algebra} over $R$, which is defined as the free noncommutative $R$-algebra generated by $\vars{x}{n},\vars{\partial}{n}$, subject to the canonical commutation relations from \cref{eq:canonical_comm}.
	
	If $R$ however has positive characteristic $p$, we observe that for $r \geq p$ 
	%
	\begin{equation}
		{\partial_i}^r {x_i}^m = m (m-1) \dots (m - r + 1) {x_i}^m = 0 \c
	\end{equation}
	%
	because a multiple of $p$ (possibly $0$) must appear in the product $m (m-1) \dots (m - r + 1)$. Hence ${\partial_i}^r f = 0$ for any polynomial $f$, so ${\partial_i}^r = 0$.
	
	If $p$ is prime and in dimension $n = 1$, an even stronger statement holds: If $D, E \in \Diff^{p-1}_{X/S}$ then $D E \in \Diff^{p-1}_{X/S}$, i.\,e. differential operators of order lesser than $p$ (in particular those of order 1) fail spectacularly at generating $\Diff_{X/S}$. To prove the claim, it suffices to show that for $g \in A$ and $k, m < p$, the order of $\dred{k} g \dred{m}$ is strictly lesser than $p$. We compute
	%
	\begin{align}
		\dred{k} g \dred{m} &= g \dred{k} \dred{m} + \left[\dred{k}, g \right] \dred{k} \\
		&= g \dred{k} \dred{m} +  \sum_{j = 0}^{k-1} h_j \dred{j} \dred{k} \c
	\end{align}
	%
	for some $h_j \in A$, and reduce the claim to showing $\ord\left( \dred{k} \dred{m} \right) < p$ for $k, m < p$. Observe that
	%
	\begin{equation}
		\dred{k} \dred{m} = \binom{n+m}{n} \dred{n+m} 
	\end{equation}
	%	
	and either $n + m < p$ or $p | \binom{n + m}{n}$, showing what we required.
	
	It is clear that this phenomenon generalizes to dimension $n > 2$: If $D, E \in \Diff_{X/S}$ are $A$-linear combinations of $\dred{\alpha}$ with $\alpha_i < p$ for all $i$, then so is $E D$. In particular we observe that elements of $\Diff^{p-1}_{X/S}$ fail to generate any differential operator order $\geq n(p-1)$.
\end{rem}


% !TeX spellcheck = en_US

\subsection{Global Properties of Sheaves of Differentials}
In this section we will globalize the two exact sequences from \cref{thm:cotangent,thm:conormal}, as well as \cref{thm:diff_tensor} to corresponding statements about the sheaves of differentials on schemes.

\begin{prop}[relative cotangent sequence, {\cite[16.4.19]{ega4.4}}] \label{thm:cotangent_sheaf}
	If $g: X \to Y$ is a $S$-morphism, the canonical maps defined in \cref{rem:Pn_diff_funct} yield an exact sequence of $\OO_X$-modules
	%
	\begin{equation}
		g^* \Omega^1_{Y/S} \xrightarrow{g^*} \Omega^1_{X/S} \to \Omega^1_{X/Y} \to 0 \label{eq:rel_cotangent_seq}
	\end{equation}
	%
\end{prop}
\begin{proof}
	Exactness is checked locally, so \wl we can assume $X = \Spec B$, $Y = \Spec A$ and $S = \Spec R$. By \cref{eq:diff_loc}, the sequence (\ref{eq:rel_cotangent_seq}) corresponds to a sequence of $B$-modules
	%
	\begin{equation}
		B \otimes_A \Omega^1_{A/R} \to \Omega^1_{B/R} \to \Omega^1_{B/A} \to 0 \p \label{eq:rel_cotangent_local}
	\end{equation}
	%
	in which all morphism are compatible with the corresponding differentials. Since the morphisms resulting from the functoriality of (affine) Kähler differentials (see \cref{rem:diff_funct}) are the only such morphisms, we deduce that (\ref{eq:rel_cotangent_local}) is the sequence from \cref{thm:cotangent} and hence exact.
\end{proof}

\begin{prop}[conormal sequence, {\cite[16.4.19]{ega4.4}}] \label{thm:conormal_sheaf}
	Let $j : Z \to X$ be a closed immersion corresponding to the ideal sheaf $\SI$. Then we have an exact sequence of $\OO_Z$-modules
	%
	\begin{equation}
		j^*\mkern-2mu \SI \xrightarrow{j^*\mkern -1mu\!\d} j^* \Omega^1_{X/S} \to \Omega^1_{Z/S} \to 0 \p
	\end{equation}
	%
	In particular $j^*\mkern -1mu \!\d$ is well defined%
	\footnote{While we are guaranteed by the theory of sheaves of sets that $j^{-1} \d$ is well-defined, this does not extend to $j^* \d$, since $\d$ is not $\OO_X$-linear.}
	and $\OO_X$-linear. The second morphism comes from functoriality.
\end{prop}
\begin{rem}
	In the situation above, $j^*\mkern-2mu \SI$ is more commonly denoted by $\SI\mkern-2mu/\mkern-1mu\SI^2$. This is equivalent, since $j^*\mkern-2mu \SI$ is isomorphic to the ``restriction'' $j^{-1} \mkern-2mu\SI\mkern-2mu/\mkern-1mu\SI^2$.
\end{rem}
\begin{proof}
%	As above, we may work locally and the claim reduces to \cref{thm:conormal}.
	For the well-definedness
	and linearity of $j^*\mkern -1mu \!\d$ observe that for $k \in j^{-1}\SI(U)$, $a \in j^{-1}\OO_X(U)$, $\ovl{b} \in \OO_Z(U)$ for some open $U \subset Z$
	%
	\begin{align}
		j^*\mkern -1mu \!\d(a k \otimes \ovl{b}) &\stackrel{\mathrm{def}}{=} \d(a k) \otimes \ovl{b} \\
			&= a \d k \otimes \ovl{b} + k \d a \otimes \ovl{b} \\
			&= a \d k \otimes \ovl{b} \\
			&= j^* \mkern -1mu \!\d(k \otimes a \ovl{b}) \p
	\end{align}
	%
	($\d a$ and $\d k$ are defined because $k$ and $a$ are locally restrictions of sections of $\OO_X$ and $\SI$ respectively).
	Now recall that $\PP^1_{X/S} = \OO_X \oplus \Omega^1_{X/S}$ (cf. \cref{rem:P1_split}), which will allow us to use \cref{thm:Pn_closed_imm}. $T^1_{X/S} = i + \d$, so by \cref{eq:pn_closed_imm_kernel} and using that $\Omega^1_{X/S} \cdot \Omega^1_{X/S} = 0$ in $\PP^n_{X/S}$, we get
	%
	\begin{align}
		j^* \left( T^1_{X/S}(\SI) \right)_{\mkern -2mu \OO_X}
			&= j^{-1} \left( \big( \!(i + \d)(\SI) \big)_{\mkern -2mu \OO_X}  \middle/ i(\SI) (i + \d)(\SI) \right) \\
			&= j^{-1} \left( \big( \!\d(\SI) \big)_{\mkern -2mu \OO_X} \otimes_{\OO_X} \OO_X/\SI \right) \\
			&= j^{-1} \left( \big( \!(i + \d)(\SI) \big)_{\mkern -2mu \OO_X} \otimes_{\OO_X} \OO_X/\SI \right) \\
			&= j^*\big( \!\d(\SI) \big)_{\mkern -2mu \OO_X} = \im\left(  j^* \mkern -1mu \!\d|_{j^* \mkern -2mu\SI} \right)
	\end{align}
	%
	(in the last step we use the linearity of $j^* \mkern -1mu \!\d$ to conclude that sections of the form $j^*(a \d b)$ lie in the image of $j^* \mkern -1mu \!\d|_{j^* \mkern -2mu\SI}$).
\end{proof}

Of course, we could also have proven this like \cref{thm:cotangent_sheaf} by reducing it to the affine case and invoking \cref{thm:conormal}.

The following proposition is the analogue to a well-known differential-geometric fact: The (co)tangent space at a point $(p, q)$ of a product of manifolds $M \times N$ is the direct sum of the (co)tangent spaces of $M$ at $p$ and $N$ at $q$. Actually something stronger holds: The (co)tangent bundle of $X \times Y$ is isomorphic to the direct sum of the pullbacks of the (co)tangent bundles of $X$ and $Y$.

\begin{prop}[differentials of fiber products, {\cite[16.4.23]{ega4.4}}] \label{thm:diff_product}
	Let $X, Y$ be to $S$-Schemes and let $p: X \times_S Y \to X$, $q: X \times_S Y \to Y$ be the canonical projections. Then (using the notation from \cref{rem:Pn_diff_funct})
	%
	\begin{equation}
		p^* \oplus q^*: p^* \Omega^1_{X/S} \oplus q^* \Omega^1_{Y/S} \longiso \Omega^1_{X \times_S Y/S}
	\end{equation}
	%
	is an isomorphism.
\end{prop}
\begin{proof}
	Isomorphy can be checked on affine opens, where this reduces to \cref{thm:diff_tensor} because the sheaves of differentials are quasi-coherent (see \cref{rem:Pn_diff_local_target}).
\end{proof}

% !TeX spellcheck = en_US
\section{Sheaves of Principal Parts and Differentials} \label{sec:sheaves}

Just like the differential of a function in multivariate analysis naturally generalizes to the differential of a function on a smooth manifold, the module Kähler differentials and algebra of principal parts naturally generalize to sheaves of modules on a scheme. As usual in algebraic geometry, we will work from a ``relative point of view'' examining a morphism of schemes $f: X \to S$.

%The main reference for this section is \cite[§ 16]{ega4.4}. 

\subsection{Definition and First Remarks}

\begin{defn}
	The diagonal morphism $\Delta_{X/S} : X \to X \times_S X$ is a (locally closed) immersion%, so there is an open subscheme $i: W \to X \times_S X$ such that $\Delta_{X/S}$ factors as
	, so we have a short exact sequence of ${\Delta_{X/S}}^{-1} \OO_{X \times_S X}$-modules
	%
	\begin{equation}
		0 \to \II = \II_{X/S} \to {\Delta_{X/S}}^{-1} \OO_{X \times_S X} \xrightarrow{\Delta_{X/S}^\sharp} \OO_X \to 0 \p
	\end{equation}
	%
	\begin{enumerate}[(i)]
	\item We call $\PP^n_{X/S} = {\Delta_{X/S}}^{-1} \OO_{X \times_S X} / \II^{n+1}$ the \demph{sheaf of principal parts} of \demph{order} $n$ of $X$ relative to $S$.
	
	$X \times_S X$ is equipped with canonical projections $p_1, p_2 : X \times_S X \to X$ which carry maps of sheaves of rings $p_i^\sharp : p_i^{-1}\OO_X \to \OO_{X \times_S X}$.
	Since ${\id_X}^{-1} = (p_i \circ \Delta_{X/S})^{-1} = {\Delta_{X/S}}^{-1} {p_i}^{-1}$, applying ${\Delta_{X/S}}^{-1}$ and factorizing the codomain by $\II^{n+1}$ gives two canonical morphisms of $f^{-1} \OO_S$-algebras
	%
	\begin{equation}
	   	i, \T^n_{X/S}: \OO_X \to {\Delta_{X/S}}^{-1} (\OO_{X \times_S X}) / \II^{n+1} = \PP^n_{X/S} \c
	\end{equation}
	%
	where $i$ comes from $p_1$ and $\T^n$ from $p_2$. As in \cref{def:Pn}, we regard $\PP^n_{X/S}$ as an $\OO_X$-algebra via $i$. (Note that $T^n_{X/S}$ induces another $\OO_X$-module structure on $\PP^n_{X/S}$.)
	Again, for $n \geq m$, we have canonical morphisms of $\OO_X$-algebras $\aug^n_m: \PP^n_{X/S} \epi \PP^m_{X/S}$ such that $\aug^n_m \circ \T^n_{X/S} = \T^m_{X/S}$.
	
	
	\item We call the $\OO_X$-module $\Omega^1_{X/S} = \II / \II^2$ the \demph{sheaf of relative differentials} of $X$ over $S$. We observe that $\d = \d_{X/S} = T^1_{X/S} - i$ maps $\OO_X$ into $\Omega^1_{X/S}$, since $\Delta_{X/S}^\sharp \circ i = \Delta_{X/S}^\sharp \circ \T^n = \id_{\OO_X}$.
	\end{enumerate}	
	
\end{defn}

\begin{rem} \label{rem:P1_split}
	It is evident from the definition that 
	%
	\begin{equation}
		0 \to \Omega^1_{X/S} \to \PP^1_{X/S} \xrightarrow{\aug^1} \OO_X \to 0
	\end{equation}
	%
	is split-exact, since $i$ splits $\pi^n$ (so does $T^1_{X/S}$, but only as a morphism of $f^{-1} \OO_S$-modules).
\end{rem}

\begin{rem}[functoriality] \label{rem:Pn_diff_funct}
	One can quickly verify the functoriality of these constructions: 
	If 
	%
	\begin{equation}
		\begin{tikzcd}[sep=large]
			X \arrow{d}{f} & X' \arrow{l}{u} \arrow{d}{f'}  \\
			S & S' \arrow{l}{v}
		\end{tikzcd}
	\end{equation}
	%
	is commutative, it induces a canonical morphisms 
	of $\OO_{X'}$-algebras $P^n(u, v): u^* \PP^n_{X/S} \to \PP^n_{X'/S'}$ such that $P^n(u, v) \circ \T^n_{X/S} = T^n_{X'/S'}$. Hence it also induces a morphism of $\OO_{X'}$-modules $w: u^* \Omega^1_{X/S} \to \Omega^1_{X'/S'}$ satisfying $w \circ u^* d_{X/S} = d_{X'/S'}$ (because $\Omega^1_{X/S} = \ker \pi^1$). Again, if $v = \id$ we denote the induced morphism $w$ by $u^*$.
\end{rem}

\begin{rem}[local situation] \label{rem:Pn_diff_local_target} 
%	We need to verify that the above definition is independent of the choice for the open set $W \subset X \times_S X$.
	Let $i: W \to X \times_S X$ be an open subscheme, such that $\Delta_{X/S}$ factors as $i \circ j$ with $j: X \to W$ a closed immersion corresponding to a quasi-coherent sheaf of ideals $\overline{\II} \subset \OO_W$.
	Then the sheaf $\II$ from above satisfies $\II = j^{-1} \overline{\II}$ and $\Omega^1_{X/S} = j^* \overline{\II}$.
	In particular this shows that $\Omega^1_{X/S}$ is quasi-coherent (and even coherent if $X$ is locally noetherian).

	%$I_{A/R}/I_{A/R}^{n+1} \subset \nil(P^n_{A/R})$ implies that $\Spec A = \Spec P^n_{A/R}$. Therefore $X^{(n)} = (X, \PP^n_{X/S})$ is not only a ringed space, but also a $S$-Scheme.
	$\overline{\II}^{n+1}$ also is a quasi-coherent sheaf of ideals, defining a closed subscheme $Z$ of $X \times_S X$. Since $\overline{\II}^{n+1}$ and $\overline{\II}$ have the same support, $X$ and $Z$ have the same underlying topological space, so $Z = (X, \PP^n_{X/S})$. Furthermore, $\overline{\II}^{n+1} \subset \overline{\II}$ implies that the inclusion $X \mono X \times_S X$ factors over $Z$.

\label{rem:Pn_diff_local}
	If $V = \Spec R \subset S$ and $U = \Spec A \subset f^{-1}(V)$ are affine, we can choose $W = U \times_V U$ obtaining $\overline{\II}|_{U \times_V U} = \widetilde{I_{A/R}}$.
	This implies
	%
	\begin{equation}
		\PP^n_{X/S}(U) = P^n_{A/R} \label{eq:Pn_affine}
	\end{equation}
	%
	as $A$-algebras and 
	%
	\begin{equation}
		\T^n_{X/S}(U) = \T^n_{A/R} \c
	\end{equation}
	%
	because $\PP^n_{X/S}$ is the structure sheaf of the closed subscheme corresponding to $\overline{\II}|^{n+1}_{U \times_V U}$.
	
	Furthermore
	%
	\begin{equation}
		\Omega^1_{U/V} = \left( \Omega^1_{A/R} \right)^\sim \label{eq:diff_loc}
	\end{equation}
	%
	and the $\d_{A/R} = \d_{U/V}$ glue to $d_{X/S}$. 
	(We actually just proved \cref{thm:diff_basic}\cref{thm:diff_basic:loc} again -- by offloading all the work to theorems about quasi-coherent sheaves.)
\end{rem}

\begin{rem} \label{rem:symmetry}
	The \demph{canonical symmetry} $s = (p_2, p_1): X \times_S X \to X \times_S X$ is an (involutive) isomorphism with the property that $s \circ \Delta_{X/S} = \Delta_{X/S}$ (locally, this is the symmetry of the tensor product in $A \otimes_R A / I_{A/R}^{n+1}$). It therefore induces an isomorphism of sheaves of abelian groups $\sigma: \PP^n_{X/S} \to \PP^n_{X/S}$, which permutes the two $\OO_X$-module structures on $\PP^n_{X/S}$. That is, if we equip the first $\PP^n_{X/S}$ with the conventional module structure and the second $\PP^n_{X/S}$ with that by $T^n_{X/S}$ (and vice-versa), $\sigma$ is a $\OO_X$-linear isomorphism.
\end{rem}


%We now proceed to prove a global version of \cref{thm:Pn_loc}.
%For any sheaf one defines on a scheme, the most important question one asks about it concerns its quasi-coherence.
Whenever one encounters a new sheaf on ascheme, a natural first question to ask is whether it is quasi-coherent. Thanks to \cref{thm:Pn_loc}, we can give a positive answer for the sheaf of principal parts:
\begin{prop}(partially {\cite[16.4.16]{ega4.4}}) \label{thm:Pn_qcoh}
	For all $n$, the sheaves $\PP^n_{X/S}$ are quasi-coherent $\OO_X$-modules (with both the conventional structure and that induced by $\T^n_{X/S}$) and for every $x \in X$
	%
	\begin{equation}
		\PP^n_{X/S, x} = P^n_{\OO_{X, x} / \OO_{S, f(x)}}
	\end{equation}
	%
\end{prop}
\begin{proof}
	This immediately reduces to the affine case as in \cref{rem:Pn_diff_local}, so \wl assume that $S = \Spec R$ and $X = \Spec A$ as in \cref{rem:Pn_diff_local}. Then $\PP^n_{X/S}(X) = P^n_{A/R}$ and using \cref{thm:Pn_loc} we obtain for $t \in A$
	%
	\begin{equation}
		\Gamma\left(D(t), \left( P^n_{A/R} \right)^\sim \right) = \left( P^n_{A/R} \right)_t = P^n_{A_t / R} = \Gamma \left( D(t), \PP^n_{X/S} \right) \p
	\end{equation}
	%
	The claim about the structure induced by $\T^n_{X/S}$ follows from symmetry.
	
	The second assertion quickly follows from the first: Let $\pp$ be the prime ideal corresponding to $x$ and $\phi: R \to A$ the homomorphism corresponding to $X \to S$. Then
	%
	\begin{equation}
		\PP^n_{X/S, x} = P^n_{A/R, \pp} = P^n_{A_\pp, R_{\phi^{-1}(\pp)}}
	\end{equation}
	%
	by the second assertion of \cref{thm:Pn_loc}.
\end{proof}


\begin{ex}[principal parts of affine space] \label{ex:Pn_affinespace}
	\cref{thm:Pn_qcoh} and the above remarks allow us to translate \cref{rem:interp_Pn} into the setting of Schemes. We see that with $A = R[\vars{x}{n}]$ and $S = \Spec R$
	%
	\begin{equation}
		\PP^k_{\AA^n_R / S} = \left( A[\vars{\epsilon}{n}] / (\vars{\epsilon}{n})^{k+1} \right)^\sim
	\end{equation}
	%
	is a free $\OO_{\AA^n_R}$-module of rank $\binom{n + k}{k}$. We further know that on global sections $T^k_{\AA^n_R / S}$ is given by \cref{eq:Tn_taylor}. Since $T^k_{\AA^n_R / S}$ is a homomorphism of $\OO_{\AA^n_R}$-algebras, on a principal open subset $D(u) \subset \AA^n_R, u \in A$, one must have $T^k_{\AA^n_R / S} (u^{-k} a) = T^k_{\AA^n_R / S}(u)^{-k} T^k_{\AA^n_R / S}(a)$. To get an explicit expression for $T^k_{\AA^n_R / S}(u)^{-1}$, observe that
	%
	\begin{align}
		T^k_{\AA^n_R / S}(u)^{-1} &=
			\left( \sum_{|\alpha| \leq k} \underline{\epsilon}^\alpha \dred{\alpha} u \right)^{-1}
		  = u^{-1} \left( 1 + u^{-1} \smashoperator{\sum_{1 \leq |\alpha| \leq k}} \underline{\epsilon}^\alpha \dred{\alpha} u \right)^{-1} \\
		  &= u^{-1} \sum_{m = 0}^k \left(\!- u^{-1} \smashoperator{\sum_{1 \leq |\alpha| \leq k}} \underline{\epsilon}^\alpha \dred{\alpha} u \right)^{\!\!m} \p
	\end{align}
	%
	(The last step is basically a series expansion in $\epsilon$ where we use that the ``$O(\epsilon^{k+1})$''-terms vanish.)
\end{ex}
% !TeX spellcheck = en_US

\subsection{Kähler differentials of field extensions}
As it just is a special case of an algebra over a ring, we can consider the module of differentials of a field extension.  While this may not be the most interesting case geometrically, it is particularly easy to describe the vector space of differentials. In fact, this section's main theorem basically tells us everything about the module of differentials $\Omega^1_{L/k}$ of a field extension $L/k$ of characteristic zero.

\begin{lem}[{\cite[16.15]{eisenbud}}] \label{thm:diff_field_tensor}
	Let $k$ be an $R$-algebra and a field and $L / k$ an algebraic and separable field extension. Then
	%
	\begin{equation}
		\Omega_{L/R} = L \otimes_k \Omega^1_{k / R}
	\end{equation}
	%
\end{lem}
\begin{proof}
	It suffices to prove the claim for finite field extensions.
	Assuming that, the general case can be handled as follows: We observe that
	%
	\begin{equation}
		L = \smashoperator{\bigcup_{S \subset L \text{~finite}}} k(S) = \smashoperator{\varinjlim_{S \subset L \text{~finite}}} k(S) 
	\end{equation}
	%
%	Both $- \otimes_k \Omega^1_{k/R}$ and $\Omega^1_{-/R}$ commute with colimits by right-adjointness and \cref{thm:diff_colim}, so it suffices to prove the claim for finite field extensions.
	and therefore, since $\Omega^1_{-/R}$ commutes with colimits by \cref{thm:diff_colim}
	\begin{align}
		\Omega^1_{L/R} &= \Omega^1_{\varinjlim_{S} k(S) / R} = \varinjlim_{S} L \otimes_{k(S)} \Omega^1_{k(S)/R} \\
		&= \varinjlim_{S} L \otimes_k \Omega^1_{k / R} = L \otimes \Omega^1_{k/R}
	\end{align}
	
	In the finite case, we choose a primitive root $\gamma$ of $L/k$ with minimal polynomial $f$, so $L \cong k[x] / f$. The conormal sequence (\cref{thm:conormal}) for $R \to k[x] \to L$ then is
	%
	\begin{equation}
		(f)/(f)^2 \xrightarrow{\bar{\d}} \Omega^1_{k[x] / R} \otimes_{k[x]} L \to \Omega^1_{L/R} \to 0 \label{eq:some_conormal}
	\end{equation}
	%
	Using $k[x] \cong k \otimes_R R[x]$, \cref{thm:diff_tensor} shows that
	%
	\begin{equation}
		\Omega^1_{k[x] / R} \otimes_{k[x]} L \cong \left( k[x] \otimes \Omega_{k/R} \oplus k[x] \d x \right) \otimes_{k[x]} L
		\cong L \otimes \Omega_{k/R} \oplus L \d x \p
	\end{equation}
	%
	Let $\pi_2$ denote the projection $L \otimes \Omega_{k/R} \oplus L \d x \to L \d x$. Since $f'(\gamma) \neq 0$ by separability, $\pi_2\left( \bar{\d} \bar{f} \right) = f'(\gamma) \d x$ generates $L \d x$ and hence the claim follows from the exactness of (\ref{eq:some_conormal}).
\end{proof}


\begin{thm}[{\cite[16.14]{eisenbud}}] \label{thm:diff_field_ext}
	Let $L / k$ be a field extension of characteristic $0$ and $\{x_i\}_{i \in I} \subset L$. Then $\{\d x_i\}_{i \in I}$ is a basis of $\Omega_{L/k}$ as $L$-vector space iff $\{x_i\}_{i \in I}$ is a transcendence basis of $L$ over $k$.
\end{thm}

\begin{rem}
An analogous statements holds in positive characteristic if we replace ``transcendence basis'' with ``$p$-basis'', see \cite{eisenbud}.
\end{rem}

\begin{proof}
	Set $K = k(\{x_i\}_{i \in I})$.
	
	(1) ``$\Leftarrow$'': If $\{x_i\}_{i \in I}$ is a transcendence basis of $L/k$, then $L/K$ is algebraic. Hence by \cref{thm:diff_field_tensor}, 
	%
	\begin{equation}
		\Omega^1_{L/k} = L \otimes_K \Omega^1_{K/k} \p
	\end{equation}
	%
	Since $K$ is the localization at the zero ideal of $k[\{x_i\}_{i \in I}]$, by \cref{thm:diff_basic}\cref{thm:diff_basic:poly} we obtain the claim, since
	%
	\begin{equation}
		L \otimes_K \Omega^1_{K/k} = L \otimes_K \bigoplus_{i \in I} K \d x_i  = \bigoplus_{i \in I} L \d x_i
	\end{equation}
	%
	In case that $I$ is infinite, we additionally need to use that $\Omega^1_{-/k}$ commutes with direct limits and
	%
	\begin{equation}
		\varinjlim_{J \subset I \text{~finite}}\; \bigoplus_{i \in J} L \d x_i = \bigoplus_{i \in I} L \d x_i
		\p
	\end{equation}
	%
	
	(2) ``$\Rightarrow$'': Suppose that $\{\d x_i\}_{i \in I}$ is a basis of $\Omega_{L/k}$. The cotangent sequence (\cref{thm:cotangent}) of $k \to K \to L$ is
	%
	\begin{equation}
		L \otimes_K \Omega^1_{K/k} \to \Omega^1_{L/k} \to \Omega^1_{L/K} \to 0 \p
	\end{equation}
	%
	Since $\{\d x_i\}_{i \in I}$ lies in the image of the leftmost map and generates $\Omega^1_{L/k}$, $\Omega^1_{L/K} = 0$ by exactness.
	Let $\{y_j\}_{j \in J} \subset L$ be a  transcendence basis of $L$ over $K$, so by (1), $\{\d y_j\}_{j \in J}$ is a basis of $\Omega^1_{L/K} = 0$. Hence $J = \emptyset$, i.\,e.\  $L$ is algebraic over $K$.
	
	Now suppose some $x_1$ is $k$-algebraically dependent on $\{x_i\}_{i \in I \setminus \{1\}}$. Then $L$ is also algebraic over $k(\{x_i\}_{i \in I \setminus \{1\}})$ and by (1), since $\{x_i\}_{i \in I \setminus \{1\}}$ contains a transcendence basis of $L/k$, $\d x_i$ lies in the $L$-span of $\{\d x_i\}_{i \in I \setminus \{1\}}$. But this contradicts the hypothesis that $\{\d x_i\}_{i \in I}$ is a basis, hence $\{x_i\}_{i \in I}$ is algebraically independent over $k$, proving the claim.
	%, i.\,e.\ there is a polynomial $f \in k[\vars{t}{n}]$ such that $f(x_1, x_2, \dots, x_n) = 0$ for some $x_2, \dots x_n \in \{x_i\}_{i \in I}$. Then $0 = \d\left( f(x_1, x_2, \dots, x_n) \right)$ is a $L$-linear combination of $\d x_1
\end{proof}


\begin{ex}
	\Cref{thm:diff_field_ext} gives us a good way to compare Kähler differentials to the ``ordinary'' differentials encountered in Analysis. We do this by considering the field extension $\MM/ \CC$, where $\MM$ denotes the field of meromorphic functions on $\CC$. In Analysis the differential of a meromorphic function $f$ is the meromorphic differential form $f' \mkern -2mu \d z$ (technically $z$ denotes the identity function). We denote their $\MM$-vector space by $\Omega^{1, \text{an}}_\MM$ and have that $\Omega^{1, \text{an}}_\MM = (\d z)_\MM$ is 1-dimensional.
		
	The algebraic situation is quite different%
	\footnote{Of course, in an algebraic setting it would be more sensible to consider $\CC(z)/\CC$, which yields results analogous to the analytic ones.}:
	If we pick a transcendence basis $T$ of $\MM/\CC$, then, by \cref{thm:diff_field_ext}, $\{\d f \mid f \in T\}$ is a basis of $\Omega^1_{\MM/\CC}$ as $\MM$-vector space. For example we could pick $z$ and $\exp$ to be two elements of $T$ (since $\exp$ is a transcendental function). As a result $\d z$ and $\d \exp$ are $\MM$-linearly independent, so algebraically $\d \left( \exp \right) \neq \exp \d z$. In fact, $\{ \exp(\alpha z) \mid |\alpha| = 1 \}$ is algebraically independent over $\CC$,
	so $\dim_\MM \Omega^1_{\MM/\CC} = \infty$.
	
	%This is actually not that surprising: Kähler differentials are a purely algebraic construction
\end{ex}



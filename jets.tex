% !TeX spellcheck = en_US

\subsection{Sheaves of Jets}
\begin{defn}
	Let $\FF$ be a $\OO_X$-module. The $\PP^n_{X/S}$-module
	%
	\begin{equation}
		\PP^n_{X/S} (\FF) = \PP^n_{X/S} \otimes_{\OO_X} \FF \c \label{eq:def_PnF}
	\end{equation}
	%
	where the tensor product is formed using the $\OO_X$-module structure on $\PP^n_{X/S}$ given by $\T^n$ is called the $n$-th \demph{jet bundle} of $\FF$ (this is sometimes denoted by $J^n \mkern -1mu \FF$). $\PP^n_{X/S}$ is equipped with a map of sheaves $\T^n_\FF: \FF \to \PP^n_{X/S} (\FF)$ mapping $s \mapsto 1 \otimes s$.
\end{defn}

\begin{rem} \label{rem:jets}
	The concept of jet bundles also exists for differentiable manifolds: Let $(E, \pi, M)$ be a smooth vector bundle of dimension $m$ on a manifold of dimension $n$. We denote the stalk of the sheaf of sections of $\pi$ at the point $p \in M$ by $\Gamma(p)$ . The stalk $C^\infty (p)$ of the sheaf of smooth functions on $M$ at $p$ is a local ring, whose maximal ideal $\mm_p$ corresponds to those functions which vanish at $p$.
	Obviously, $\Gamma(p)$ is a $C^\infty(p)$-module.
	We call the image of a local section $\sigma \in \Gamma(p)$ under the map 
	%
	\begin{equation}
		j^r_p: \Gamma(p) \to \Gamma(p) / \mm_p^{r+1} \Gamma(p)
	\end{equation}
	%
	its $r$-jet at $p$.	
%	We say that two local sections $\sigma, \tau \in \Gamma(p)$ have the same $r$-jet at $p$ if 
%	%
%	\begin{equation}
%		\sigma \equiv \tau \mod \mm_p^{r+1} \p
%	\end{equation}
%	%

	There is a more explicit description of this: Two local sections $\sigma, \tau \in \Gamma(p)$ have the same $r$-jet iff in some (or equivalently, in every) coordinate system $(x_i)_i$ around $p$ 
	%
	\begin{equation}
		\left. \partial^\alpha \sigma \right|_p = \left. \partial^\alpha \tau \right|_p \label{eq:jets_with_derivatives}
	\end{equation}
	%
	for all multiindices $\alpha \in \NN^n$ with $|\alpha| \leq r$. So $j^r_p \sigma$ carries the information of the value of $\sigma$ and its derivatives of order $\leq r$ at $p$.
	This shows that $J^r_p E = \Gamma(p) / \mm_p^{r+1} \Gamma(p)$ is a $m \binom{n + r}{r}$-dimensional vector space and that
	%
	\begin{equation}
		J^r E = \coprod_{p \in M} J^r_p E
	\end{equation}
	%
	has a natural topology and smooth structure making it a vector bundle. We get a $\RR$-linear mapping of sections $j^r : \Gamma(E) \to \Gamma(J^r E)$ by setting $(j^r \sigma) (p) = j^r_p (\sigma)$.
	Furthermore, we have $J^r E = E \otimes J^r \mathbf{R}$ where $\mathbf{R} = \RR \times M$ denotes the trivial line bundle on $M$.
%	The equivalence class of $\sigma$ under the corresponding equivalence relation is denoted by $j^r_p(\sigma)$ and called its $r$-jet at $p$.
	
	This is completely analogous to to our algebraic situation when $S = \Spec k$: $j^r$ corresponds to the map $\T^r_{X/S}$, and the fiber of $J^r E$ at $p$ is $\Gamma(p) / \mm_p^{r+1} \Gamma(p)$, while the ``fiber'' of $\PP^r_{X/S}(\FF)$ at $x$ is
	%
	\begin{equation}
		\left( \PP^r_{X/S}(\FF) \right)_x \otimes_{\OO_{X, x}} \kappa(x) \cong \FF_x / \mm_x^{r+1} \FF_x
	\end{equation}
	%
	by \cref{eq:Pn_fiber}. The algebraic construction however is more general since the sheaf $\FF$ need not be locally free of finite rank (but the vector bundle $E$ is locally trivial and has finite dimension).
	
	When $S \neq \Spec k$ is perceived as a geometric object, we interpret -- using \cref{thm:Pn_fibers} -- $\PP^n_{X/S}(\FF)$ as a bundle of jets along the fibers of $f: X \to S$: Only the ``derivatives'' of a function $t$ along the fibers contribute to its ``jet''/``Taylor polynomial'' $T^n_{X/S}(t)$.
	
\end{rem}

% ----------------------------------------------------------------------------------------

The next lemma, a generalization of \cref{thm:Pn_qcoh} shows that the functor $\FF \mapsto \PP^n_{X/S}(\FF)$ is well-behaved in terms of presentations of $\FF$.
\begin{lem}[{\cite[16.7.4]{ega4.4}}] \label{thm:PnF_presentations}
	If $\FF$ is quasi-coherent, then so is $\PP^n_{X/S}(\FF)$, for both its conventional $\OO_X$-module structure and that induced by $T^n_{\FF}$. If furthermore $\FF$ is locally of finite type (finite presentation) and $f$ is locally of finite type (resp.\ finite presentation), then $\PP^n_{X/S}(\FF)$ is locally of finite type (resp.\ finite presentation) for both its structures as $\OO_X$-module.
\end{lem}
\begin{proof}
	%For the module structure induced by $T^n_{\FF}$, the first claim follows directly from \cref{thm:Pn_qcoh,eq:def_PnF}. \cref{thm:Pn_qcoh} however also states that for an $R$-algebra $A$
	In all cases the hypotheses for $\FF$ can be formulated as a local presentation
	%
	\begin{equation}
		\OO_U^I \to \OO_U^J \to \FF|_U \to 0 \c
	\end{equation}
	%
	 on some open $U \subset X$, with $J$ finite if $\FF$ is locally of finite type and both $I$ and $J$ finite if $\FF$ is locally of finite presentation. By right exactness of $\PP^n_{U/S} \otimes_{\OO_X} -$, where the tensor product is done w.\,r.\,t.\ the module structure induced by $T^n_{U/S}$, this gives us an exact sequence of $\PP^n_{U/S}$-modules
	 %
	 \begin{equation}
	 	\left( \PP^n_{U/S} \right)^I \xrightarrow{\alpha} \left( \PP^n_{U/S} \right)^J \xrightarrow{\beta} \PP^n_{U/S}(\FF|_U) \to 0 \p
	 \end{equation}
	 %
	 In particular this is also an exact sequence of $\OO_X$-modules with respect to \emph{both} module structures.
	 The arguments for both structures are the same, so pick and we fix one structure for the rest of the proof.	 
	 Now since $( \PP^n_{U/S} )^I$ and $( \PP^n_{U/S} )^I$ are quasi-coherent, their cokernel $\PP^n_{U/S}(\FF|_U)$ is too.
	 
	 In case that $f$ is locally of finite type, we can assume (by shrinking $U$ if necessary) that $P^n_{U/S}$ is of finite type by \cref{thm:Pn_sheaf_gen}. Therefore $(\PP^n_{U/S})^J$ is of finite type ($J$ being finite in this case), and since it surjects onto $\PP^n_{U/S}(\FF|_U)$, the latter is finite, too.
	 
	 In case that $f$ is locally of finite presentation, we can assume that $\PP^n_{U/S}$ is of finite presentation by \cref{thm:Pn_lfp}. Hence $\PP^n_{U/S}(\FF|_U)$ is the quotient of the finitely presented module $( \PP^n_{U/S} )^J$ by $\ker \beta = \im \alpha$ which is finite since $(\PP^n_{U/S})^I$ is finite ($I$ being finite too in this case). By a standard result in commutative algebra, this implies that $\PP^n_{U/S}(\FF|_U)$ is of finite type.
\end{proof}

% ----------------------------------------------------------------------------------------

The next, seemingly technical, lemma is actually very intuitive:
%According to \cref{rem:jets}, we view sections of $\PP^n_{X/S}$ as Taylor-series-valued functions. Then it is to be expected that the sheaf of order $m$ Taylor series of such functions, i.\,e.\  $\PP^m_{X/S}(\PP^n_{X/S})$, contains the sheaf of order $n + m$ Taylor series $\PP^{n + m}_{X/S}$.
Until now, we regard second-order approximations as an improved version of first-oder approximations. In analysis one can however also view second-order approximation as a first-order approximation to the first-order approximation of a function. This is basically equivalent to the (not entirely trivial) fact that the derivative of the derivative of a function is its second-order coefficient (i.\,e.\ its second derivative).
Of course this is not restricted to first-order approximations, the $m$-th order approximation of the $n$-th order approximation of a function is equivalent to its $n + m$-th order approximation.

\begin{lem} \label{thm:higher_der}
	For $n, m \in \NN$, there is a unique injective homomorphism of $\OO_X$-algebras $\delta : \PP_{X/S}^{n+m} \mono \PP^n_{X/S}(\PP_{X/S}^m)$ such that the diagram
	%
	\begin{cd}
			\OO_X \dar[swap]{\T^n_{X/S}} \rar{\T^{n + m}_{X/S}} & \PP^{n + m}_{X/S} \dar{\delta} \\
			\PP^n_{X/S} \rar[swap, outer sep=2pt]{\T^m_{\PP^n_{X/S}}} & \PP^m_{X/S}(\PP^n_{X/S})
			\label[cd]{dg:higher_der}
	\end{cd}
	%
	is commutative.
\end{lem}

\begin{proof}
	The uniqueness of such a $\delta$ follows from  \cref{thm:sheaf_gen}(\ref{thm:sheaf_gen:det}) because the image of $\T^{n+m}_{X/S}$ generates $\PP^{n+m}_{X/S}$ by \cref{thm:Pn_sheaf_gen}. Hence it suffices to show that such $\delta$ exist locally, that is for affine $X$ and $S$. By the uniqueness this morphisms can then be glued to obtain the global $\delta$. So let $X = \Spec A$ and $S = \Spec R$ and write $I = I_{A/R}$.
	The injectivity of $\delta$ is immediate if one considers that $T^n_{X/S}$, $T^m_{\PP^n_{X/S}}$ and $T^{n+m}_{X/S}$ are both injective (they have left inverses $\aug^n$, $\aug^m \otimes 1$ and $\aug^{n+m}$).	
	All involved sheafs are quasi-coherent by \cref{thm:Pn_qcoh,thm:PnF_presentations}, so $\delta$ corresponds to a homomorphism of $A$-algebras
	%
	\begin{equation}
		\left( A \otimes_R A \right) / I^{n+m+1} \longto \left( \left( A \otimes_R A \right) / I^{n+1} \right) \otimes_A \left( \left( A \otimes_R A \right) / I^{m+1} \right)
	\end{equation}
	%
	where we view both sides as $A$-algebras w.\,r.\,t.\ the leftmost $A$ and the $A$-tensor product on the right is taken w.\,r.\,t.\ the second $A$ on its left and the first $A$ on its right factor. Equivalently, we can search for a homomorphism of $A$-algebras $\phi$ from $A \otimes_R A$ into the same target that is zero on $I^{n+m+1}$. The condition from \cref{dg:higher_der} can then be expressed as 
	%
	\begin{equation}
		\forall a \in A: \quad \phi(1 \otimes a) = (\ovl{1 \otimes 1}) \otimes (\ovl{1 \otimes a}) \p \label{eq:higher_der:comm}
	\end{equation}
	%
	We claim that the map given by 
	%
	\begin{equation}
		\phi(a \otimes b) = (\ovl{a \otimes 1}) \otimes (\ovl{1 \otimes b})\;, \qquad a, b \in A
	\end{equation}
	%
	satisfies the above conditions. \cref{eq:higher_der:comm} is obviously satisfied. We further observe that 
	%
	\begin{align}
		\phi(1 \otimes a - a \otimes 1) &= (\ovl{1 \otimes 1} \otimes \ovl{1 \otimes a}) - (\ovl{a \otimes 1}) \otimes (\ovl{1 \otimes 1}) \\
										&= (\ovl{1 \otimes a} - \ovl{a \otimes 1}) \otimes (\ovl{1 \otimes 1}) - (\ovl{1 \otimes a}) \otimes (\ovl{1 \otimes 1})
											+ (\ovl{1 \otimes 1}) \otimes (\ovl{1 \otimes a}) \\
										&= (\ovl{1 \otimes a} - \ovl{a \otimes 1}) \otimes (\ovl{1 \otimes 1})
											+ (\ovl{1 \otimes 1}) \otimes (\ovl{1 \otimes a} - \ovl{a \otimes 1}) \c
	\end{align}
	%
	because $(\ovl{1 \otimes a}) \otimes (\ovl{1 \otimes 1}) = (\ovl{1 \otimes 1}) \otimes (\ovl{a \otimes 1})$.
	Since $\phi$ is a homomorphism of rings, this implies that for $a_i \in A$
	%
	\begin{equation}
		\phi \left( \prod_{i=0}^{n+m} (1 \otimes a_i - a_i \otimes 1) \right) =
			\quad \oldsmashoperator{\sum_{H \subseteq \{0, \dots, n+m\}}} \hspace{1.9em}
					\prod_{i \in H} (\ovl{1 \otimes a_i} - \ovl{a_i \otimes 1}) \otimes \prod_{i \not\in H} (\ovl{1 \otimes a_i} - \ovl{a_i \otimes 1}) \p
	\end{equation}
	%
	By the pigeonhole principle, in every summand of the right-hand side there are either at lest $n+1$ factors in the left or at least $m+1$ factors in the right factor, hence the right-hand side is zero. By \cref{thm:gen}, this is all we had to show for $\phi$ to be zero on $I^{n+m+1}$, and the claim follows.
\end{proof}

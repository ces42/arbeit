% !TeX spellcheck = de_DE
\documentclass[12pt,]{article}
\usepackage[utf8]{inputenc}

% ------------------------------------------------------------------------------
% import packages
% ------------------------------------------------------------------------------

% math packages
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathtools} % for example for mathrlap
\usepackage{thmtools}
\usepackage{mathrsfs} % for \mathscr
\usepackage{tikz-cd} % for commutative diagrams
\usepackage{amsfonts}
\usepackage{mleftright} % remove extra spacing before \left and after \right

%referencing
\usepackage[breaklinks]{hyperref} % clickable references
\usepackage[capitalize]{cleveref} % easier references
\usepackage{autonum} %to not display unused numbered equations:

\usepackage{enumerate} % for lowercase roman numerals in enumerate

% font
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{tgtermes} % font that is almost identical to times new roman
\usepackage{microtype}

% ------------------------------------------------------------------------------
% setup and options
% ------------------------------------------------------------------------------

% penalties for breaking in math mode
\binoppenalty=840 % default 700
\relpenalty=600 % default 500

% Margins and other formatting
\setlength{\voffset}{-28mm}
\setlength{\hoffset}{-1in}
\setlength{\topmargin}{19mm}
\setlength{\oddsidemargin}{25mm}
\setlength{\evensidemargin}{25mm}
\setlength{\textwidth}{160mm}
%\setlength{\parindent}{0pt}

\flushbottom % use all availabale vertical space on a page

\setlength{\textheight}{230mm}
\setlength{\footskip}{20mm}
\setlength{\headsep}{50pt}
\setlength{\headheight}{0pt}

% Typography
\mleftright % define \left to \mleft and \right to \mright (tighter spacing)

% tikzcd setup
\tikzcdset{every label/.append style = {font = \small}}
\tikzcdset{sep=large}
\tikzset{
    arrowdecor/.style={anchor=south, rotate=90, inner sep=.5mm}
}

% AMSthm setup
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\Crefname{lem}{Lemma}{Lemmas}
\newtheorem{prop}[thm]{Proposition}
\Crefname{prop}{Proposition}{Propositions}
\newtheorem{cor}[thm]{Corollary}

\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition}
\newtheorem*{notation}{Notation}

\theoremstyle{remark}
\newtheorem{rem}[thm]{Remark}
\newtheorem{ex}[thm]{Example}


% references
\numberwithin{equation}{thm}

\Crefname{cd}{Diagram}{Diagrams}
\Crefformat{cd}{Diagram~(#2#1#3)}

% environment for commutative diagrams. Does not automatically integrate into cleveref :/
\newenvironment{cd}{%
	\begin{equation}
	\begin{tikzcd}[sep=large]
}{%
	\end{tikzcd}
	\end{equation}
}

% no indent after ending a proof/theorem
\makeatletter
\let\oldendproof\endproof
\def\endproof{\oldendproof\aftergroup\@afterindentfalse\aftergroup\@afterheading}
\makeatother


% ------------------------------------------------------------------------------
% custom commands
% ------------------------------------------------------------------------------

%labelling
\Crefformat{item}{(#2#1#3)}
\newcommand\llabel[1]{\label[item]{\currentprefix:#1}\label[item]{\thethm:#1}}
\newcommand{\lref}[1]{\cref{\thethm:#1}}

% arrows
\newcommand{\epi}{\twoheadrightarrow}
\newcommand{\mono}{\hookrightarrow}
\newcommand{\iso}{\xrightarrow{\sim}}
\newcommand{\longiso}{\xrightarrow{~\sim~}}
\newcommand{\longto}{\longrightarrow}

% define shortcuts for common symbols
\renewcommand{\phi}{\varphi}
\renewcommand{\epsilon}{\varepsilon}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\RR}{\mathbb{R}}
\renewcommand{\AA}{\mathbb{A}} % \AA used to be A with circle
\newcommand{\MM}{\mathcal{M}}
\newcommand{\OO}{\mathcal{O}}
\newcommand{\II}{\mathscr{I}}
\newcommand{\PP}{\mathcal{P}}
\newcommand{\FF}{\mathscr{F}}
\newcommand{\GG}{\mathscr{G}}
\newcommand{\HH}{\mathscr{H}}
\newcommand{\pp}{\mathfrak{p}}
\newcommand{\mm}{\mathfrak{m}}

\renewcommand{\l}{\ell} % \l used to be that (polish?) dashed l

\newcommand{\SI}{\mathscr{K}}

% math operators
\DeclareMathOperator{\chr}{char}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\sHom}{\mathscr{H}\mkern -4.5mu \mathit{om}}
\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\colim}{colim}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\nil}{nil}
\DeclareMathOperator{\im}{Im}
\DeclareMathOperator{\coker}{Coker}
\DeclareMathOperator{\xker}{Ker}
\renewcommand{\ker}{\xker}
\DeclareMathOperator{\Diff}{Dif\mkern -1.5mu f}
\DeclareMathOperator{\sDiff}{\mathscr{D}\mkern -1.5mu \mathit{iff}}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\ord}{ord}

% shorthands
\newcommand{\pd}[1]{\frac{\partial}{\partial#1}}
\newcommand{\pdiff}[3]{\frac{\partial^{#3} #1}{\partial #2^#3}}
\newcommand{\vars}[2]{#1_1, \dots, #1_#2}
\newcommand{\lfrac}[2]{#1 / #2}
\newcommand{\floor}[1]{\lfloor #1 \rfloor}
\newcommand{\fl}{\floor{\,\cdot\,}}

% functions I might want to rename later
\newcommand{\T}{T}
\renewcommand{\d}{\mathop{}\!d}
\newcommand{\dred}[1]{\partial^{\left[#1\right]}}
\newcommand{\aug}{\pi}

% other decisions 
\newcommand{\demph}{\textbf}

% points and commas for displaymath 
\newcommand{\p}{\mathrlap{~~~\text{.}}}
\renewcommand{\c}{\mathrlap{~~~\text{,}}}

% commands for abbreviations in the text
\newcommand{\wl}{w.\,l.\,o.\,g.\ }

% custom typography
\let\oldsmashoperator\smashoperator
\renewcommand{\smashoperator}[1]{\;\oldsmashoperator{#1}\;}

\makeatletter
\newcommand{\ovl}[1]{%
  \overline{\raisebox{0pt}[\dimexpr\height+0.6pt\relax]{\m@th$#1$}}%
  % Package `calc' can be used as alternative for `\dimexpr'.
}
\makeatother




\begin{document}
\pagestyle{empty}
%%%% Title page
\begin{titlepage}
\begin{center}
\includegraphics{TUMlschwarz.png}\\[3mm]
\sf
{\Large
  Technische Universität München\\[5mm]
  Department of Mathematics\\[8mm]
}
\normalsize
\includegraphics{TUMlMschwarz.png}\\[15mm]

Bachelor's Thesis\\[15mm]

{\Huge
  Differential Operators on Schemes
}
\bigskip

\normalsize
Carlos Esparza Sánchez
\end{center}
\vspace*{75mm}

Supervisor: Prof. Dr. Christian Liedtke
\medskip

Advisor: Prof. Dr. Christian Liedtke
\medskip

Submission Date: November 19th 2019

\end{titlepage}
\vspace*{150mm}

I assure the single handed composition of this bachelor's thesis only supported by declared resources.
\bigskip

Garching, 
\newpage
%%%% Zusammenfassung in deutscher Sprache
\section*{Zusammenfassung}

Diese Arbeit ist hauptsächlich eine Zusammenfassung von Kapitel 16 aus \cite{ega4.4}. Sie beschäftigt sich primär mit den Garben von Hauptteilen (principal parts) und Differentialoperatoren auf einem Schema. Im Vergleich zum Original wird versucht die Beweise weiter auszuarbeiten sowie Beispiele und Interpretation zu liefern. Letzteres geschieht vor allem durch einen Vergleich mit den entsprechenden Objekten in der Differentialgeometrie. Bei der Einführung und Untersuchung der Garben von Hauptteilen wird parallel die Garbe von Kähler-Differentiale eingeführt und ihre Eigenschaften auf die der Garben von Hauptteilen zurückgeführt. Zu Beginn werden die Garben von Hauptteilen und Kähler-Differentialen im affinen Fall untersucht.

\newpage
\tableofcontents
\newpage

%%%% Page numbering restarts here
\pagenumbering{arabic}
\pagestyle{headings}


% ------------------------------------------------------------------------------
% Einleitung
% ------------------------------------------------------------------------------
\input{pre.tex}

% ------------------------------------------------------------------------------
% commutative Algebra
% ------------------------------------------------------------------------------
\input{pn.tex}
\input{kaehler.tex}
\input{kaehler_field.tex}

% ------------------------------------------------------------------------------
% principal parts und Differentiale auf Schemata
% ------------------------------------------------------------------------------
\input{pn_sheaf_intro.tex}
\input{pn_sheaf_thms.tex}
\input{kaehler_sheaf.tex}
\input{jets.tex}

% ------------------------------------------------------------------------------
% Differentialoperatoren
% ------------------------------------------------------------------------------
\input{diffoperatoren.tex}


\bibliographystyle{amsalpha}
\bibliography{BA.bib}
\end{document}



